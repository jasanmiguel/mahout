
import java.io.*;
import java.util.*;
 
import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.eval.IRStatistics;
import org.apache.mahout.cf.taste.eval.RecommenderBuilder;
import org.apache.mahout.cf.taste.eval.RecommenderEvaluator;
import org.apache.mahout.cf.taste.eval.RecommenderIRStatsEvaluator;
import org.apache.mahout.cf.taste.impl.eval.AverageAbsoluteDifferenceRecommenderEvaluator;
import org.apache.mahout.cf.taste.impl.eval.GenericRecommenderIRStatsEvaluator;
import org.apache.mahout.cf.taste.impl.eval.RMSRecommenderEvaluator;
import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.impl.recommender.GenericItemBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.PearsonCorrelationSimilarity;
import org.apache.mahout.cf.taste.impl.similarity.EuclideanDistanceSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.recommender.Recommender;
import org.apache.mahout.cf.taste.similarity.ItemSimilarity;
 
public class Recom {
 
  private static void pearson(double train) throws Exception {
      File userPreferencesFile = new File("/home/ubuntu/ratings1M.csv");
      DataModel model = new FileDataModel(userPreferencesFile);

      RecommenderEvaluator absEvaluator = new AverageAbsoluteDifferenceRecommenderEvaluator();
      RecommenderEvaluator rmseEvaluator = new RMSRecommenderEvaluator();
      RecommenderBuilder recommenderBuilder = new RecommenderBuilder() {
      public Recommender buildRecommender(DataModel model)
          throws TasteException {
        ItemSimilarity similarity = new PearsonCorrelationSimilarity(model);
        return new GenericItemBasedRecommender(model,similarity);
      }
    };
      

      double score = absEvaluator.evaluate(recommenderBuilder, null, model, train, 1.0);
      double rmse = rmseEvaluator.evaluate(recommenderBuilder, null, model, train, 1.0);
      System.out.println(score + " - " + rmse);

      RecommenderIRStatsEvaluator evaluator = new GenericRecommenderIRStatsEvaluator();
      // Build the same recommender for testing that we did last time:
      /*
      IRStatistics stats = evaluator.evaluate(recommenderBuilder, null,
          model, null, 2,
          GenericRecommenderIRStatsEvaluator.CHOOSE_THRESHOLD, 0.5);
      System.out.println("euclidean precision " + stats.getPrecision());
      System.out.println("euclidean recall " + stats.getRecall());  
      */  
    
  }

  private static void euclidean(double train) throws Exception {
      File userPreferencesFile = new File("/home/ubuntu/ratings1M.csv");
      DataModel model = new FileDataModel(userPreferencesFile);

      RecommenderEvaluator absEvaluator = new AverageAbsoluteDifferenceRecommenderEvaluator();
      RecommenderEvaluator rmseEvaluator = new RMSRecommenderEvaluator();
      RecommenderBuilder recommenderBuilder = new RecommenderBuilder() {
      public Recommender buildRecommender(DataModel model)
          throws TasteException {
        ItemSimilarity similarity = new EuclideanDistanceSimilarity(model);
        return new GenericItemBasedRecommender(model,similarity);
      }
    };

      double score = absEvaluator.evaluate(recommenderBuilder, null, model, train, 1.0);
      double rmse = rmseEvaluator.evaluate(recommenderBuilder, null, model, train, 1.0);
      System.out.println(score + " - " + rmse);

      RecommenderIRStatsEvaluator evaluator = new GenericRecommenderIRStatsEvaluator();
      // Build the same recommender for testing that we did last time:
      /*
      IRStatistics stats = evaluator.evaluate(recommenderBuilder, null,
          model, null, 2,
          GenericRecommenderIRStatsEvaluator.CHOOSE_THRESHOLD, 0.5);
      System.out.println("euclidean precision " + stats.getPrecision());
      System.out.println("euclidean recall " + stats.getRecall());  
      */  

  }  

  /**
   * @param args
   * @throws Exception
   */
  public static void main(String[] args) throws Exception {
    int LOOP = 10;
    for(double j= 0.7; j<=0.9; j=j+0.1){
      System.out.println("PEARSON: "+j);
      for(int i = 0; i<=LOOP; i++){
        pearson(j);
      }
      System.out.println("EUCLIDEAN: "+j);
      for(int i = 0; i<=LOOP; i++){
        euclidean(j);
      }      
    }
  }
}
