import org.apache.spark.mllib.recommendation.ALS
import org.apache.spark.mllib.recommendation.MatrixFactorizationModel
import org.apache.spark.mllib.recommendation.Rating

object ALSRecommenderEvaluator extends App 
{
	val data = sc.textFile("/home/ubuntu/ratings1M.csv")
	val ratings = data.map(_.split(',') match { case Array(user, item, rate) =>
		Rating(user.toInt, item.toInt, rate.toDouble)
	})

	var LOOP = 0;
	var ITERATIONS = 0;

	for( LOOP <- 1 to 10)
	{
		for( ITERATIONS <- 1 to 5)
		{
			val rank = 2 * LOOP
			val numIterations = 5 * ITERATIONS
			val model = ALS.train(ratings, rank, numIterations, 0.01)

			val usersProducts = ratings.map { case Rating(user, product, rate) =>
				(user, product)
			}
			val predictions = 
				model.predict(usersProducts).map { case Rating(user, product, rate) => 
				((user, product), rate)
			}
			val ratesAndPreds = ratings.map { case Rating(user, product, rate) => 
				((user, product), rate)
			}.join(predictions)

			val MSE = ratesAndPreds.map { case ((user, product), (r1, r2)) => 
				val err = (r1 - r2)
				err * err
			}.mean()

			println("LOOP: "+LOOP+" RANK: "+rank+" ITERATIONS: "+numIterations+  " MSE: " + MSE)
		}
	}

}