-- MySQL Workbench Synchronization
-- Generated: 2015-06-01 16:20
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Pepe

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE TABLE IF NOT EXISTS `movilens`.`user` (
  `user_id` INT(11) NOT NULL,
  `gender` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `movilens`.`movie` (
  `movie_id` INT(11) NOT NULL,
  `title` VARCHAR(45) NULL DEFAULT NULL,
  `type` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`movie_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `movilens`.`user_has_movie` (
  `user_id` INT(11) NOT NULL,
  `movie_id` INT(11) NOT NULL,
  `stars` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`, `movie_id`),
  INDEX `fk_user_has_movie_movie1_idx` (`movie_id` ASC),
  INDEX `fk_user_has_movie_user_idx` (`user_id` ASC),
  CONSTRAINT `fk_user_has_movie_user`
    FOREIGN KEY (`user_id`)
    REFERENCES `movilens`.`user` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_has_movie_movie1`
    FOREIGN KEY (`movie_id`)
    REFERENCES `movilens`.`movie` (`movie_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
